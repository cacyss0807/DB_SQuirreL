



-- ---
-- Globals
-- ---

-- SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
-- SET FOREIGN_KEY_CHECKS=0;

-- ---
-- Table 'athlete'
-- 
-- ---

DROP TABLE IF EXISTS `athlete`;
		
CREATE TABLE `athlete` (
  `id` INTEGER AUTO_INCREMENT,
  `first_name` varchar(255) NOT NULL,
  `dob` DATE NULL DEFAULT NULL,
  `photo` varchar(255) NULL DEFAULT NULL,
  `country` varchar(255) NOT NULL,
  `sport` varchar(255) NOT NULL,
  `middle_name` varchar(255) NULL,
  `last_name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
);

-- ---
-- Table 'country'
-- 
-- ---

DROP TABLE IF EXISTS `country`;
		
CREATE TABLE `country` (
  `name` varchar(255) NOT NULL,
  `capital` varchar(255) NULL DEFAULT NULL,
  PRIMARY KEY (`name`)
);

-- ---
-- Table 'sport'
-- 
-- ---

DROP TABLE IF EXISTS `sport`;
		
CREATE TABLE `sport` (
  `name` varchar(255) NOT NULL,
  `description` varchar(500) NULL DEFAULT NULL,
  `season` ENUM('Summer','Winter','Both') NOT NULL,
  `photo` varchar(255) NULL DEFAULT NULL,
  PRIMARY KEY (`name`)
);

-- ---
-- Table 'olympic'
-- 
-- ---

DROP TABLE IF EXISTS `olympic`;
		
CREATE TABLE `olympic` (
  `id` INTEGER NOT NULL AUTO_INCREMENT,
  `season` ENUM('Summer','Winter','Both') NOT NULL,
  `year` INTEGER NOT NULL,
  `city` varchar(255) NULL,
  `country` varchar(255) NOT NULL DEFAULT 'NULL',
  PRIMARY KEY (`id`)

);

-- ---
-- Table 'medal'
-- 
-- ---

DROP TABLE IF EXISTS `medal`;
		
CREATE TABLE `medal` (
  `athlete_id` INTEGER NOT NULL,
  `olympic_id` INTEGER NOT NULL,
  `type` ENUM('Gold','Silver','Bronze') NOT NULL,
  `sport` varchar(255) NOT NULL,
   PRIMARY KEY (athlete_id, olympic_id, sport)
);

-- ---
-- Table 'olympic_athletes'
-- 
-- ---

DROP TABLE IF EXISTS `olympic_athletes`;
		
CREATE TABLE `olympic_athletes` (
  `athlete_id` INTEGER NOT NULL,
  `olympic_id` INTEGER NOT NULL,
   PRIMARY KEY (athlete_id, olympic_id)
);

-- ---
-- Table 'olympic_participating_country'
-- 
-- ---

DROP TABLE IF EXISTS `olympic_participating_country`;
		
CREATE TABLE `olympic_participating_country` (
  `olympic_id` INTEGER NOT NULL,
  `country` varchar(255) NOT NULL,
   PRIMARY KEY (olympic_id, country)
);

-- ---
-- Table 'olympic_sport'
-- 
-- ---

DROP TABLE IF EXISTS `olympic_sport`;
		
CREATE TABLE `olympic_sport` (
  `olympic_id` INTEGER NOT NULL,
  `sport` varchar(255) NOT NULL,
   PRIMARY KEY (`olympic_id`)  
);

-- ---
-- Foreign Keys 
-- ---

ALTER TABLE `athlete` ADD FOREIGN KEY (country) REFERENCES `country` (`name`) ON DELETE CASCADE;
ALTER TABLE `athlete` ADD FOREIGN KEY (sport) REFERENCES `sport` (`name`) ON DELETE CASCADE;
ALTER TABLE `olympic` ADD FOREIGN KEY (country) REFERENCES `country` (`name`) ON DELETE CASCADE;
ALTER TABLE `medal` ADD FOREIGN KEY (athlete_id) REFERENCES `athlete` (`id`) ON DELETE CASCADE;
ALTER TABLE `medal` ADD FOREIGN KEY (olympic_id) REFERENCES `olympic` (`id`) ON DELETE CASCADE;
ALTER TABLE `medal` ADD FOREIGN KEY (sport) REFERENCES `sport` (`name`) ON DELETE CASCADE;
ALTER TABLE `olympic_athletes` ADD FOREIGN KEY (athlete_id) REFERENCES `athlete` (`id`) ON DELETE CASCADE;
ALTER TABLE `olympic_athletes` ADD FOREIGN KEY (olympic_id) REFERENCES `olympic` (`id`) ON DELETE CASCADE;
ALTER TABLE `olympic_participating_country` ADD FOREIGN KEY (olympic_id) REFERENCES `olympic` (`id`) ON DELETE CASCADE;
ALTER TABLE `olympic_participating_country` ADD FOREIGN KEY (country) REFERENCES `country` (`name`) ON DELETE CASCADE;
ALTER TABLE `olympic_sport` ADD FOREIGN KEY (olympic_id) REFERENCES `olympic` (`id`) ON DELETE CASCADE;
ALTER TABLE `olympic_sport` ADD FOREIGN KEY (sport) REFERENCES `sport` (`name`) ON DELETE CASCADE;

-- ---
-- Table Properties
-- ---

-- ALTER TABLE `athlete` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `country` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `sport` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `olympic` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `medal` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `olympic_athletes` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `olympic_participating_country` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
-- ALTER TABLE `olympic_sport` ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ---
-- Test Data
-- ---

-- INSERT INTO `athlete` (`id`,`first_name`,`dob`,`photo`,`country`,`sport`,`middle_name`,`last_name`) VALUES
-- ('','','','','','','','');
-- INSERT INTO `country` (`name`,`capital`) VALUES
-- ('','');
-- INSERT INTO `sport` (`name`,`description`,`season`,`photo`) VALUES
-- ('','','','');
-- INSERT INTO `olympic` (`id`,`season`,`year`,`city`,`country`) VALUES
-- ('','','','','');
-- INSERT INTO `medal` (`athlete_id`,`olympic_id`,`type`,`sport`) VALUES
-- ('','','','');
-- INSERT INTO `olympic_athletes` (`athlete_id`,`olympic_id`) VALUES
-- ('','');
-- INSERT INTO `olympic_participating_country` (`olympic_id`,`country`) VALUES
-- ('','');
-- INSERT INTO `olympic_sport` (`olympic_id`,`sport`) VALUES
-- ('','');


