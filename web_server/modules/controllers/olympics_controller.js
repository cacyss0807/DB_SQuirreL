"use strict"

// Olympics Controller

var helper = require('../controller_helpers');

module.exports = function(db) {
  return {
    index: function(req, res) {
      // TODO: Check the query
      // TODO: Create a page number option % 10
      var query = 'SELECT * FROM olympic ORDER BY year DESC';
      helper.sendDBResponse(db, query, req, res);
    },
    mapID: function (req, res, next, id) {
      if (id == null)
        res.send("OlympicID can't null");
      else {
        var query = 'SELECT * FROM olympic WHERE id = ' + id;
        // If olympicId doesn't exist, send back the res error, o.w. continue
        helper.sendToCallbackTimed(db, query, function(rows) {
          req.currOlympic = rows[0];
          next();
        }, res, 1000);
      }
    },
    summary: function(req, res) {
      var startYear = req.param('startYear');
      var endYear = req.param('endYear');


      if (startYear && endYear) {
        var query = 'SELECT * ' +
                    'FROM olympic ' +
                    'WHERE olympic.year > ' + startYear + ' and olympic.year < ' + endYear + '';

        helper.sendDBResponse(db, query, req, res);
      } else {
        res.send("startYear or endYear not found");
      }
    },
    show: function(req, res) {
      var id = req.currOlympic.id;
      // TODO: Fix Query
      var query = 'SELECT * FROM olympic WHERE id = ' + id;
      helper.sendDBResponseTimed(db, query, req, res, 3000, true);
    },
    create: function(req, res) {
      var year = req.body.year;
      var season = req.body.season;
      var city = req.body.city;
      var country = req.body.country;

      console.log(req.body);

      var query = 'INSERT into olympic(season, year, city, country) ' +
                  'VALUES (' + '\'' + season + '\'' + ', ' + year + ', ' + '\'' + city + '\'' + ', ' +  '\'' + country + '\'' + ')';
      helper.sendDBResponseTimed(db, query, req, res, 3000);
    },
    update: function(req, res) {
      var id = req.currOlympic.id;
      var year = req.body.year;
      var season = req.body.season;
      var city = req.body.city;
      var country = req.body.country;

      var query = 'UPDATE olympic ' +
                  'SET city='+ '\'' + city + '\'' + ', ' +
                  'country=' + '\'' + country + '\'' + ', ' +
                  'season='+ '\'' + season + '\'' + ', ' +
                  'year=' + year + ' ' +
                  'WHERE id=' + id;

      helper.sendDBResponseTimed(db, query, req, res, 1000);
    },
    delete: function(req, res) {
      var id = req.currOlympic.id;
      var query = 'DELETE ' +
                  'FROM olympic ' +
                  'WHERE id = ' + id;

      helper.sendDBResponseTimed(db, query, req, res, 1000);
    },
    participatedCountries: function(req, res) {
      var startYear = req.param('startYear');
      var endYear = req.param('endYear');
      var sport_name = req.param('sportName');

      if (startYear && endYear && sport_name) {
        var query = 'SELECT DISTINCT(country_name) ' +
                    'FROM olympic_participating_country INNER JOIN olympic on (olympic_participating_country.olympic_id = olympic.id) ' +
                    ' INNER JOIN olympic_sport on (olympic_sport.olympic_id = olympic.id) ' +
                    'WHERE olympic.year >= ' + startYear + ' and olympic.year <= ' + endYear + ' ' +
                    ' ' + (sport_name != "ALL" ? ("AND olympic_sport.sport = " + '\'' + sport_name + '\'') : " " );


        console.log(query);
        helper.sendDBResponse(db, query, req, res);
      } else {
        res.status(500);
        res.send("startYear or endYear not found");
      }
    }

  };
}
