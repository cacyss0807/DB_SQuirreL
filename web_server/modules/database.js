"use strict"


// Database Connection Configs
//if ('development' == app.get('env')) {
    var db_config = require('./config/db/development.js').config;
//} else {
//    var db_config = require('./config/db/production.js').config;
//}

// Requrie the node-mysql library
var mysql = require('mysql');
var db_pool = mysql.createPool(db_config);

// Require the Q Promise Library
var Q = require('q');

// Returns a Promise
// Use with care, sometimes the Promise may never resolve
var doQuery = function(query) {
    var deferred = Q.defer();

    db_pool.getConnection(function(err, connection) {
        if (err) {
            console.log("Error Occurred while connecting to the database (database.js)");
            deferred.reject(new Error(err));
            connection.release();
        } else {
            connection.query(query, function(err, rows) {
                if (err) {
                    console.log("Error occurred while making a query (database.js)");
                    deferred.reject(new Error(err));
                } else {
                  if (rows)
                    deferred.resolve(rows);
                  else
                    deferred.resolve([]);
                }
                connection.release();
            });
        }
    });

    return deferred.promise;
};

// Returns a Promise that will resolve in ms time if there is no response
// From the database
var doTimedQuery = function(query, ms) {
    var promise = doQuery(query);
    return promise.timeout(ms, "Error timeout occurred");
};

module.exports.doQuery = doQuery;
module.exports.pool = db_pool;
module.exports.doTimedQuery = doTimedQuery;
