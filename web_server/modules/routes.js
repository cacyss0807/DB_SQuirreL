"use strict"


module.exports = function(app, db) {

    // Require all middlewares here
    var Auth = require('./RESTAuth.js')(db);
    console.log(Auth);


    // Require all the controllers here
    // NOTE: We pass our DB object to share the DB connection Pool
    var olympics_controller = require('./controllers/olympics_controller.js')(db);
    var athletes_controller = require('./controllers/athletes_controller.js')(db);
    var medals_controller = require('./controllers/medals_controller.js')(db);
    var sports_controller = require('./controllers/sports_controller.js')(db);
    var countries_controller = require('./controllers/countries_controller.js')(db);
    var sessions_controller = require('./controllers/sessions_controller.js')(db);

    // REST API for Olympics
    // Step 1: Map the URL Parameters to the mappers
    app.param(':olympicId', olympics_controller.mapID);
    app.param(':athleteId', athletes_controller.mapID);
    app.param(':sportId', sports_controller.mapID);
    app.param(':countryId', countries_controller.mapID);
    // Step 2: Actions for each unique URL
    app.get('/olympics', olympics_controller.index);
    app.get('/olympics/summary', olympics_controller.summary);
    app.get('/olympics/:olympicId', olympics_controller.show);
    app.get('/olympics/:olympicId/athletes', athletes_controller.index);
    app.get('/olympics/athletes/summary', athletes_controller.summary);
    app.get('/olympics/:olympicId/athletes/:athleteId', athletes_controller.show);
    app.get('/olympics/:olympicId/athletes/:athleteId/results', medals_controller.show);
    app.get('/olympics/:olympicId/medals', medals_controller.index);
    app.get('/olympics/medals/summary', medals_controller.summary);
    app.get('/olympics/:olympicId/sports', sports_controller.index);
    app.get('/olympics/:olympicId/sports/:sportId', sports_controller.show);
    app.get('/olympics/sports/summary', sports_controller.summary);
    app.get('/olympics/countries/summary', olympics_controller.participatedCountries);

    // Requires admin authorization
    app.post('/olympics', Auth.authenticate_token, olympics_controller.create);
    app.put('/olympics/:olympicId', Auth.authenticate_token, olympics_controller.update);
    app.delete('/olympics/:olympicId', Auth.authenticate_token, olympics_controller.delete);

    // Sessions
    app.post('/sessions/login', sessions_controller.login);
    app.get('/sessions/checkToken', Auth.authenticate_token, sessions_controller.checkToken);

    // Countries Controller
    app.get('/countries', countries_controller.index);

    // Sports Controller
    app.get('/sports', sports_controller.index);

};
