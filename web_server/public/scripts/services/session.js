

'use strict';

angular.module('olympicDashboard')
    .service('SessionService', function($window, $http) {
        this.tokenExists = function() {
            if ($window.sessionStorage.token) {
                return true;
            } else {
                return false;
            }
        };

        this.getToken = function() {
            return $window.sessionStorage.token;
        };

        this.setToken = function(tok) {
            delete $window.sessionStorage.token;
            $window.sessionStorage.token = tok;
        };
    });
