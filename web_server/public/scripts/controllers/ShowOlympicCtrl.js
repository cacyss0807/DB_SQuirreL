
'use strict';

// Show Olympcis Ctrl (Angular)

angular.module('olympicDashboard')
    .controller('ShowOlympicCtrl', function($scope, Restangular, $location, currOlympic, ISO3166) {
        $scope.olympic = currOlympic;

        $scope.athlete_countries = {};
        $scope.medal_countries = {};
        $scope.athletes = [];
        $scope.medals = [];
        $scope.olympicAthletes = {
          regions: [],
          markers: [],
          coordinates: [],
          markerData: []
        };
        $scope.olympicMedals = {
          regions: [],
          markers: [],
          coordinates: [],
          markerData: []
        };
        $scope.currPath = $location.path();

        currOlympic.all('athletes').getList().then(function(res) {
          $scope.athletes = res;
          var currCountries = {};
          var currAthletes = {};

          angular.forEach(res, function(val, key) {
            if (currCountries[val.country]) {
              currCountries[val.country].athlete_count += 1;
              currAthletes[ISO3166.getCountryCode(val.country)] += 1;
            } else {
              currCountries[val.country] = { athlete_count: 1};
              currAthletes[ISO3166.getCountryCode(val.country)] = 1;
            }
          });

          var res = angular.copy($scope.olympicAthletes);
          res.regions = [];
          res.regions.push(currAthletes);
          $scope.olympicAthletes = res;

        }, function(err) {
          console.log("Error occurred cannot retrieve athletes.");
        });

        currOlympic.all('medals').getList().then(function(res) {
          $scope.medals = res;
          var currCountries = {};
          var currMedals = {};

          angular.forEach(res, function(val, key) {
  			if (currCountries[val.country]) {
              currCountries[val.country].medal_count += 1;
              currMedals[ISO3166.getCountryCode(val.country)] += 1;
            } else {
              currCountries[val.country] = { medal_count: 1};
              currMedals[ISO3166.getCountryCode(val.country)] = 1;
            }
          });

          var res = angular.copy($scope.olympicMedals);
          res.regions = [];
          res.regions.push(currMedals);
          $scope.olympicMedals = res;

          $scope.medal_countries = currCountries;


        }, function(err) {
          console.log("Error occurred cannot retrieve medals.");
        });

        $scope.deleteOlympic = function() {
          Restangular.one('olympics', $scope.olympic.id).remove().then(function(res) {
            console.log("Delete Succeeded");
            console.log(res);
            $location.path('/admin/olympics');
          }, function(err) {
            console.log("Error Occurred");
            console.log(err);
          });
        };

        $("#link2-1").click(function(){
  		$("#panel2-2").removeClass("map-hide");
         $("#tab2-1").addClass("active");
         $("#tab2-2").removeClass("active");
         $("#panel2-1").removeClass("content");
  		 $("#panel2-1").addClass("content active");
  		 $("#panel2-2").removeClass("content active");
  		 $("#panel2-2").addClass("content");
		});

		$("#link2-2").click(function(){
		$("#panel2-2").removeClass("map-hide");
  		 $("#tab2-2").addClass("active");
  		 $("#tab2-1").removeClass("active");
  		 $("#panel2-2").removeClass("content");
  		 $("#panel2-2").addClass("content active");
  		 $("#panel2-1").removeClass("content active");
  		 $("#panel2-1").addClass("content");});
    	});
